<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	protected const ADMIN = 1;
	protected const PEMOHON = 2;
	protected const KABID = 3;
	protected const SURVEY = 4;

	public function __construct()
	{
		parent::__construct();
	}

	protected function is_login()
	{
		return null !== $this->session->userdata('username');
	}

	protected function is_admin()
	{
		return $this->session->userdata('role') == static::ADMIN;
	}

	protected function is_pemohon()
	{
		return $this->session->userdata('role') == static::PEMOHON;
	}

	protected function is_kabid()
	{
		return $this->session->userdata('role') == static::KABID;
	}

	protected function is_survey()
	{
		return $this->session->userdata('role') == static::SURVEY;
	}

	protected function redirect_if_authenticated()
	{
		if (!$this->is_login()) {
			return redirect(base_url('auth/login'));
		}

		if ($this->is_admin()) {
			return redirect(base_url('admin'));
		}

		if ($this->is_kabid()) {
			return redirect(base_url('kabid'));
		}

		if ($this->is_survey()) {
			return redirect(base_url('survey'));
		}

		return redirect(base_url('pemohon'));
	}

	protected function redirect_if_not_authenticated()
	{
		if (!$this->is_login()) {
			return redirect(base_url('auth/login'));
		}
	}

	public function redirect_if_not_admin()
	{
		if (!$this->is_admin()) {
			return show_404();
		}
	}

	public function redirect_if_not_pemohon()
	{
		if (!$this->is_pemohon()) {
			return show_404();
		}
	}

	public function redirect_if_not_kabid()
	{
		if (!$this->is_kabid()) {
			return show_404();
		}
	}

	public function redirect_if_not_survey()
	{
		if (!$this->is_survey()) {
			return show_404();
		}
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */