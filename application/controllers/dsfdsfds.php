<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mainmodel','model');
	}
	public function index()
	{
		if ($this->session->userdata('username')===null) {
			$this->load->view('admin/v_login');
		}
	}
	public function dologin()
	{
		$cek = $this->model->dologin();
		if ($cek) {
			redirect('admin','refresh');
		}else{
			$this->session->set_flashdata('error', 'Username atau password tidak cocok!');
			redirect('login','refresh');
		}
		if($_SESSION["Captcha"]!=$_POST["nilaiCaptcha"]){
			header("location:Admin/v_signUp.php?pesan=salah");
		}else{		
			echo "<p>Captcha Anda Benar</p>";
		}
	}
	public function kembaliLogin()
	{
		$this->load->view('admin/v_login');
	}
	public function signUp()
	{
		$this->load->view('admin/v_signUp');
	}
	public function lupaPass()
	{
		$this->load->view('admin/v_lupaPass');
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login','refresh');
	}
	public function addUser()
	{
		$datauser = array(
			'username'	=> $this->input->post('username'),
			'password' 	=> $this->input->post('password'),
			'email' 	=> $this->input->post('email'));			
		$this->model->insert('tbl_user',$datauser);
		$this->session->set_flashdata('success', 'Data berhasil ditambah!');
		redirect('login','refresh');
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */