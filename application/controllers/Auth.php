<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model', 'user');
		$this->load->model('Auth_model', 'auth');
	}

	public function index()
	{
		if ($this->is_login()) {
			$this->redirect_if_authenticated();
		}

		$this->load->view('layouts/header');
		$this->load->view('auth/login', ['title' => 'Login']);
		$this->load->view('layouts/footer');
	}

	public function register()
	{
		if ($this->is_login()) {
			$this->redirect_if_authenticated();
		}

		$this->load->view('layouts/header');
		$this->load->view('auth/register', ['title' => 'Register']);
		$this->load->view('layouts/footer');
	}

	public function post_register()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('nama_lengkap', 'Nama lengkap', 'trim|required');
		$this->form_validation->set_rules('no_ktp', 'Nomor KTP', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('provinsi', 'Provinsi', 'trim|required');
		$this->form_validation->set_rules('no_hp', 'Nomor HP', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			return $this->register();
		}

		$data = $this->input->post();

		$id_user = $this->user->daftar($data);

		$q = $this->user->daftar_pemohon($data, $id_user);

		if ($q) {
			return redirect('auth/login');
		}
	}

	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			return $this->index();
		}

		if (!$this->user->check_username($username)) {
			$this->session->set_flashdata('username_check', 'Akun tidak ditemukan.');
			return $this->index();
		}

		if (!$this->user->check_password($username, $password)) {
			$this->session->set_flashdata('password_check', 'Password yang anda masukkan salah!.');
			return $this->index();
		}

		$user = $this->user->get($username, $password);
		
		$this->session->set_userdata($user);

		if ($user['role'] == static::ADMIN) {
			return redirect(base_url('admin'));
		}

		if ($user['role'] == static::SURVEY) {
			return redirect(base_url('survey'));
		}

		if ($user['role'] == static::KABID) {
			return redirect(base_url('kabid'));
		}

		$pemohon = $this->user->get_pemohon($user['id']);
		
		$this->session->set_userdata($pemohon);

		return redirect(base_url('pemohon'));
	}

	public function lupa_password()
	{
		$this->load->view('layouts/header');
		$this->load->view('auth/email_password', ['title' => 'Lupa Password']);
		$this->load->view('layouts/footer');
	}

	public function confirm_email()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

		if ($this->form_validation->run() == FALSE) {
			$this->lupa_password();
			return;
		}

		$email = $this->input->post('email');

		$user_info = $this->auth->getUserInfoByEmail($email);

		if (empty($user_info)) {
			$this->session->set_flashdata('email_check', 'Email tidak ditemukan.');
			return redirect('auth/lupa_password');
		}

		$token = $this->auth->insertToken($user_info->id);
		$qstring = $this->base64url_encode($token);
		$url = site_url('auth/reset_password/token/' . $qstring);
		$link = '<a href="' . $url . '">' . $url . '</a>';

		$message = '';             
		$message .= '<strong>Hai, anda menerima email ini karena ada permintaan untuk memperbaharui password anda.</strong><br>';  
		$message .= '<strong>Silakan klik link ini:</strong> ' . $link;

		$config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user' => '',  // Email gmail
            'smtp_pass'   => '',  // Password gmail
            'smtp_crypto' => 'ssl',
            'smtp_port'   => 465,
            'crlf'    => "\r\n",
            'newline' => "\r\n"
        ];

        $this->load->library('email', $config);
		
		$this->email->from('andihabil1004@gmail.com', 'Habil');
		$this->email->to($email);
		
		$this->email->subject('Reset password');
		$this->email->message($message);
		
		$this->email->send();
		
		echo $this->email->print_debugger();
	}

	public function reset_password()
	{
		$token = $this->base64url_decode($this->uri->segment(4));

       	$cleanToken = $this->security->xss_clean($token);

       	$user_info = $this->auth->isTokenValid($cleanToken);

       	if(!$user_info){  
			$this->session->set_flashdata('username_check', 'Token tidak valid atau kadaluarsa');  
			redirect(site_url('auth'),'refresh');
		}

		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');  
       	$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

       	if ($this->form_validation->run() == FALSE) {
       		$this->load->view('layouts/header');
            $this->load->view('auth/reset_password');
            $this->load->view('layouts/footer');
        }else{
            $post = $this->input->post(NULL, TRUE);
            $cleanPost = $this->security->xss_clean($post);
            $cleanPost['id_user'] = $user_info->id;
            unset($cleanPost['passconf']);
            if(!$this->auth->updatePassword($cleanPost)){
                $this->session->set_flashdata('sukses', 'Update password gagal.');
            }else{
                $this->session->set_flashdata('sukses', 'Password anda sudah diperbaharui. Silakan login.');
            }
            redirect(site_url('auth'),'refresh');
        }
	}

	public function logout()
	{
		$this->session->sess_destroy();

		return redirect(base_url('auth/login'));
	}

	public function base64url_encode($data) 
	{
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    public function base64url_decode($data) 
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */