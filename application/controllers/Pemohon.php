<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemohon extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->redirect_if_not_authenticated();
		$this->redirect_if_not_pemohon();

		$this->load->model('Perizinan_model', 'perizinan');
		$this->load->model('Permohonan_model', 'permohonan');
	}

	public function index()
	{
		$data['title'] = "Dasbor Pemohon";
		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/pemohon_sidebar');
		$this->load->view('pemohon/index');
		$this->load->view('layouts/footer');
	}

	public function daftar_permohonan()
	{
		$data['title'] = "Permohonan";

		$permohonan = $this->permohonan->get_by_pemohon($this->session->userdata('id_pemohon'));

		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/pemohon_sidebar');
		$this->load->view('pemohon/permohonan', ['permohonan' => $permohonan]);
		$this->load->view('layouts/footer');
	}

	public function perizinan()
	{
		$data['title'] = "Perizinan";
		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/pemohon_sidebar');
		$this->load->view('pemohon/perizinan');
		$this->load->view('layouts/footer');
	}

	public function post_perizinan()
	{
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
		$this->form_validation->set_rules('no_ktp', 'Nomor KTP', 'trim|required');
		$this->form_validation->set_rules('provinsi', 'Provinsi', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('no_hp', 'Nomor HP', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$this->perizinan();
			return;	
		}

		$data = $this->input->post();
		$data['id_pemohon'] = $this->session->userdata('id_pemohon');

		$q = $this->perizinan->post($data);

		if (!$q) {
			$this->perizinan();
			return;	
		}

		return redirect(base_url('pemohon/perizinan_berhasil'));
	}

	public function status_perizinan()
	{
		$data['title'] = "Status Perizinan";
		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/pemohon_sidebar');
		$this->load->view('pemohon/perizinan_berhasil');
		$this->load->view('layouts/footer');
	}

	public function perizinan_berhasil()
	{
		$data['title'] = "Perizinan Berhasil";
		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/pemohon_sidebar');
		$this->load->view('pemohon/perizinan_berhasil');
		$this->load->view('layouts/footer');
	}

	public function permohonan()
	{
		$data['title'] = "Permohonan";
		$data['permohonan'] = $this->permohonan->get_by_pemohon($this->session->userdata('id_pemohon'));
		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/pemohon_sidebar');
		$this->load->view('pemohon/form_permohonan');
		$this->load->view('layouts/footer');
	}

	public function daftar_perizinan()
	{
		$data['title'] = "Perizinan";
		$data['perizinan'] = $this->perizinan->get_by_pemohon($this->session->userdata('id_pemohon'));
		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/pemohon_sidebar');
		$this->load->view('pemohon/daftar_perizinan');
		$this->load->view('layouts/footer');
	}

	public function status_permohonan($id_permohonan = null)
	{
		if ($id_permohonan === null) {
			show_404();
		}
		$data['title'] = "Status Permohonan";
		$permohonan = null;
		$permohonan = $this->permohonan->get($id_permohonan)[0];
		$berkas = $this->permohonan->get_berkas_by_permohonan($id_permohonan);

		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/pemohon_sidebar');
		$this->load->view('pemohon/detail_permohonan', compact('permohonan', 'berkas', 'id_permohonan'));
		$this->load->view('layouts/footer');
	}

	public function post_permohonan()
	{
		$this->form_validation->set_rules('memiliki_imb', 'Memiliki IMB', 'trim|required');
		$this->form_validation->set_rules('bentuk_perusahaan', 'Bentuk Perusahaan', 'trim|required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('no_identitas', 'Nomor Identitas', 'trim|required');
		$this->form_validation->set_rules('nama_bangunan', 'Nama Bangunan', 'trim|required');
		$this->form_validation->set_rules('permohonan_imb', 'Permohonan IMB', 'trim|required');
		$this->form_validation->set_rules('luas_bangunan', 'Luas Bangunan', 'trim|required');
		$this->form_validation->set_rules('jumlah_lantai', 'Jumlah Lantai', 'trim|required');
		$this->form_validation->set_rules('tinggi_bangunan', 'Tinggi Bangunan', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$this->permohonan();
			return;	
		}

		$data = $this->input->post();
		$data['id_pemohon'] = $this->session->userdata('id_pemohon');

		$q = $this->permohonan->post($data);

		if (!$q) {
			$this->permohonan();
			return;	
		}

		return redirect(base_url('pemohon/daftar_permohonan'));
	}

	public function upload_berkas()
	{
		$data['title'] = "Upload Berkas Permohonan";

		$permohonan = $this->permohonan->get_by_pemohon($this->session->userdata('id_pemohon'));

		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/pemohon_sidebar');
		$this->load->view('pemohon/upload_berkas', compact('permohonan'));
		$this->load->view('layouts/footer');
	}

	public function post_upload_berkas()
	{
		if (empty($_FILES['upload_ktp']['name'])) {
		    $this->form_validation->set_rules('upload_ktp', 'upload_ktp', 'required');
		}
		
		if (empty($_FILES['upload_surat_persetujuan_rt']['name'])) {
			$this->form_validation->set_rules('upload_surat_persetujuan_rt', 'upload_surat_persetujuan_rt', 'required');
		}

		if (empty($_FILES['upload_sertifikat_tanah']['name'])) {
			$this->form_validation->set_rules('upload_sertifikat_tanah', 'upload_sertifikat_tanah', 'required');
		}

		if (empty($_FILES['upload_pbb']['name'])) {
			$this->form_validation->set_rules('upload_pbb', 'upload_pbb', 'required');
		}

		if (empty($_FILES['upload_npwp']['name'])) {
			$this->form_validation->set_rules('upload_npwp', 'upload_npwp', 'required');
		}

		$this->form_validation->set_rules('id_permohonan', 'Permohonan', 'trim|required');

		$id_permohonan = $this->input->post('id_permohonan');

		if ($this->form_validation->run() == FALSE) {
			$this->upload_berkas();
			return;
		}

		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);

		$errors = [];
		$datas = [];
		
		// KTP
		if ( ! $this->upload->do_upload('upload_ktp')){
			$errors['upload_ktp'] = $this->upload->display_errors();
		}
		else{
			$data = [
				'tipe_berkas' => 'upload_ktp',
				'lokasi_berkas' => $this->upload->data('file_name'),
				'id_permohonan' => $id_permohonan,
			];
			array_push($datas, $data);
		}

		// Surat Persetujuan RT
		if ( ! $this->upload->do_upload('upload_surat_persetujuan_rt')){
			$errors['upload_surat_persetujuan_rt'] = $this->upload->display_errors();
		}
		else{
			$data = [
				'tipe_berkas' => 'upload_surat_persetujuan_rt',
				'lokasi_berkas' => $this->upload->data('file_name'),
				'id_permohonan' => $id_permohonan,
			];
			array_push($datas, $data);
		}

		// Sertifikat Tanah
		if ( ! $this->upload->do_upload('upload_sertifikat_tanah')){
			$errors['upload_sertifikat_tanah'] = $this->upload->display_errors();
		}
		else{
			$data = [
				'tipe_berkas' => 'upload_sertifikat_tanah',
				'lokasi_berkas' => $this->upload->data('file_name'),
				'id_permohonan' => $id_permohonan,
			];
			array_push($datas, $data);
		}

		// PBB
		if ( ! $this->upload->do_upload('upload_pbb')){
			$errors['upload_pbb'] = $this->upload->display_errors();
		}
		else{
			$data = [
				'tipe_berkas' => 'upload_pbb',
				'lokasi_berkas' => $this->upload->data('file_name'),
				'id_permohonan' => $id_permohonan,
			];
			array_push($datas, $data);
		}

		// NPWP
		if ( ! $this->upload->do_upload('upload_npwp')){
			$errors['upload_npwp'] = $this->upload->display_errors();
		}
		else{
			$data = [
				'tipe_berkas' => 'upload_npwp',
				'lokasi_berkas' => $this->upload->data('file_name'),
				'id_permohonan' => $id_permohonan,
			];
			array_push($datas, $data);
		}

		$q = $this->permohonan->upload_files($datas);

		if (!$q) {
			$this->upload_berkas();
			return;	
		}

		return redirect(base_url('pemohon/daftar_permohonan'));
	}

}

/* End of file Pemohon.php */
/* Location: ./application/controllers/Pemohon.php */