<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->redirect_if_not_authenticated();
		$this->redirect_if_not_admin();

		$this->load->model('Permohonan_model', 'permohonan');
	}

	public function index()
	{
		$data['title'] = "Dashboard";
		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/admin_sidebar');
		$this->load->view('admin/index');
		$this->load->view('layouts/footer');
	}

	public function permohonan()
	{
		$data['title'] = "Permohonan";

		$permohonan = $this->permohonan->get();

		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/admin_sidebar');
		$this->load->view('admin/permohonan', ['permohonan' => $permohonan]);
		$this->load->view('layouts/footer');
	}

	public function detail_permohonan($id_permohonan = null)
	{
		if ($id_permohonan === null) {
			show_404();
		}
		$data['title'] = "Cek Berkas";
		$permohonan = null;
		$permohonan = $this->permohonan->get($id_permohonan)[0];
		$berkas = $this->permohonan->get_berkas_by_permohonan($id_permohonan);

		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/admin_sidebar');
		$this->load->view('admin/detail_permohonan', compact('permohonan', 'berkas', 'id_permohonan'));
		$this->load->view('layouts/footer');
	}

	public function verifikasi_semua_berkas($id_permohonan)
	{
		$this->db->set('tgl_verifikasi', date('Y-m-d H:i:s'));
		$this->db->where('id_permohonan', $id_permohonan);
		$this->db->update('tbl_permohonan');

		if ($this->db->affected_rows() > 0) {
			return redirect('admin/detail_permohonan/'.$id_permohonan);
		}

		return redirect('admin/detail_permohonan/'.$id_permohonan);
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */