<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabid extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->redirect_if_not_authenticated();
		$this->redirect_if_not_kabid();

		$this->load->model('Permohonan_model', 'permohonan');
	}

	public function index()
	{
		$data['title'] = "Dashboard";
		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/kabid_sidebar');
		$this->load->view('admin/index');
		$this->load->view('layouts/footer');
	}

	public function permohonan()
	{
		$data['title'] = "Permohonan";

		$permohonan = $this->permohonan->get_not_approved();

		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/kabid_sidebar');
		$this->load->view('kabid/permohonan', ['permohonan' => $permohonan]);
		$this->load->view('layouts/footer');
	}

	public function approve($id_permohonan)
	{
		$data['title'] = "Approval Permohonan";

		$permohonan = $this->permohonan->get($id_permohonan)[0];

		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/kabid_sidebar');
		$this->load->view('kabid/upload_approval', ['permohonan' => $permohonan]);
		$this->load->view('layouts/footer');
	}

	public function searchForId($id, $array) 
	{
		foreach ($array as $key => $val) {
			if ($val['tipe_berkas'] === $id) {
				return $key;
			}
		}
		return null;
	}

	public function print($id_permohonan)
	{
		$berkas = $this->permohonan->get_berkas_by_permohonan($id_permohonan);
		$index = $this->searchForId('upload_survey', $berkas);
		return redirect(base_url('uploads/' . $berkas[$index]['lokasi_berkas']));
	}

	public function upload_approval($id_permohonan)
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);

		$errors = [];
		$data = [];
		
		// KTP
		if ( ! $this->upload->do_upload('upload_approval')){
			$errors['upload_approval'] = $this->upload->display_errors();
		}
		else{
			$data = [
				'tipe_berkas' => 'upload_approval',
				'lokasi_berkas' => $this->upload->data('file_name'),
				'id_permohonan' => $id_permohonan,
			];
		}

		$this->db->set('tgl_approval', date('Y-m-d H:i:s'));
		$this->db->where('id_permohonan', $id_permohonan);
		$this->db->update('tbl_permohonan');

		if ($this->db->affected_rows() > 0 && $this->db->insert('tbl_berkas_permohonan', $data)) {
			return redirect('kabid/permohonan');
		}

		return redirect('kabid/permohonan');
	}

}

/* End of file Kabid.php */
/* Location: ./application/controllers/Kabid.php */