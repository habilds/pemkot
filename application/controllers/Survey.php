<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survey extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->redirect_if_not_authenticated();
		$this->redirect_if_not_survey();

		$this->load->model('Permohonan_model', 'permohonan');
	}

	public function index()
	{
		$data['title'] = "Dashboard";
		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/survey_sidebar');
		$this->load->view('admin/index');
		$this->load->view('layouts/footer');
	}

	public function permohonan()
	{
		$data['title'] = "Permohonan";

		$permohonan = $this->permohonan->get_not_survey();

		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/survey_sidebar');
		$this->load->view('survey/permohonan', ['permohonan' => $permohonan]);
		$this->load->view('layouts/footer');
	}

	public function survey($id_permohonan)
	{
		$data['title'] = "Permohonan";

		$permohonan = $this->permohonan->get($id_permohonan)[0];

		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/survey_sidebar');
		$this->load->view('survey/upload_survey', ['permohonan' => $permohonan]);
		$this->load->view('layouts/footer');
	}

	public function upload_survey($id_permohonan)
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);

		$errors = [];
		$data = [];
		
		// KTP
		if ( ! $this->upload->do_upload('upload_survey')){
			$errors['upload_survey'] = $this->upload->display_errors();
		}
		else{
			$data = [
				'tipe_berkas' => 'upload_survey',
				'lokasi_berkas' => $this->upload->data('file_name'),
				'id_permohonan' => $id_permohonan,
			];
		}

		$this->db->set('tgl_survey', date('Y-m-d H:i:s'));
		$this->db->where('id_permohonan', $id_permohonan);
		$this->db->update('tbl_permohonan');

		if ($this->db->affected_rows() > 0 && $this->db->insert('tbl_berkas_permohonan', $data)) {
			return redirect('survey/permohonan');
		}

		return redirect('survey/permohonan');
	}

}

/* End of file Survey.php */
/* Location: ./application/controllers/Survey.php */