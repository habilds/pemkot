<?php if($this->session->userdata('level')=="s"){?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active"><?php echo $title ?></li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $title ?></h1>
		</div>
	</div><!--/.row batas untuk pencarian anggota-->

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Data Pemohon 
				</div>
			<div class="panel-body">
				<?php 
				if ($this->session->flashdata('error')!==null) {
					?>
					<div class="alert alert-danger">
						<?php echo $this->session->flashdata('error') ?>
					</div>
					<?php
				}

				if ($this->session->flashdata('success')!==null) {
					?>
					<div class="alert alert-success">
						<?php echo $this->session->flashdata('success') ?>
					</div>
					<?php
				}
				 ?>
				 <?php if (validation_errors()) : ?>
				      <div class="alert alert-danger">
				        Username telah digunakan
				      </div>
				  <?php endif; ?>
				 

				  <div class="modal-body">
					<form method="post" action="<?php echo site_url('admin/add_permohonan') ?>">
						<div class="row">
							<input type="hidden" name="id" value="<?php echo $id_pemohon?>">
							<div class="col col-lg-12">
								<div class="form-group">
									<label>Nomor Pendaftaran </label>
									<input type="text" name="no_pendaftaran" placeholder="Nomor Pendaftaran " required="" autofocus="" class="form-control">
								</div>
								<div class="form-group">
									<label>Nama Pemohon</label>
									<input type="text" placeholder="Nama Pemohon" name="nama_pemohon" required="" autofocus=""class="form-control"></input>
								</div>
								<div class="form-group">
									<label>Nama Perusahaan</label>
									<input type="text" name="nama_perusahaan" placeholder="Nama Perusahaan" required="" autofocus="" class="form-control">
								</div>
								<div class="form-group">
									<label>Jenis Usaha</label>
									<input type="text" name="jenis_usaha" placeholder="Jenis Usaha" required="" autofocus="" class="form-control">
								</div>
								<div class="form-group">
									<label>Luas Tanah</label>
									<input type="text" name="luas_tanah" placeholder="Luas Tanah" required="" autofocus="" class="form-control">
								</div>
								<div class="form-group">
									<label>Alamat</label>
									<textarea placeholder="Alamat" name="alamat" required="" class="form-control"></textarea>
								</div>
								<div class="form-group">
									<label>Kelurahan</label>
									<input type="text" name="kelurahan" placeholder="Kelurahan" required="" autofocus="" class="form-control">
								</div>
								<div class="form-group">
									<label>Kecamatan</label>
									<input type="text" name="kecamatan" placeholder="Kecamatan" required="" autofocus="" class="form-control">
								</div>
								<div class="form-group">
									<label>Jenis Bangunan</label>
									<input type="text" name="jenis_bangunan" placeholder="Jenis Bangunan" required="" autofocus="" class="form-control">
								</div>
								<div class="form-group">
									<label>Jenis Perizinan</label>
									<input type="text" name="jenis_perizinan" placeholder="Jenis Perizinan " required="" autofocus="" class="form-control">
								</div>
								<div class="form-group">
									<label>Telepon</label>
									<input type="text" name="telepon" placeholder="Telepon" required="" autofocus="" class="form-control">
								</div>
								<button type="submit" class="btn btn-primary pull-right">Kirim</button>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
</div><!--/.row-->
</div>
<!--
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title">Add anggota</h3> <span>Id Anggota : <?php echo $id_anggota ?></span>
			</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
		</div>
	</div>
</div>

<?php 
    if ($offset == "") { $i = 0; } else { $i = $offset; }
    foreach ($query as $key) {
    	$i++;
    	?>
		<div id="edit<?php echo $i ?>" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
				<h3 class="modal-title">Add anggota</h3> <span>Id Anggota : <?php echo $key->id ?></span>
			</div>
				<div class="modal-body">
					<form method="post" action="<?php echo site_url('admin/update_anggota/'.$key->id) ?>">
						<div class="row">
							<div class="col col-lg-12">
								<div class="form-group">
									<label>Nama </label>
									<input type="text" name="nama" placeholder="Nama " required="" autofocus="" class="form-control" value="<?php echo $key->nama ?>">
								</div>
								<div class="form-group">
									<label>Pekerjaan</label>
									<textarea placeholder="pekerjaan" name="pekerjaan" required="" class="form-control"><?php echo $key->pekerjaan ?></textarea>
								</div>
								<div class="form-group">
									<label>Agama</label>
									<textarea placeholder="agama" name="agama" required="" class="form-control"><?php echo $key->agama ?></textarea>
								</div>
								<div class="form-group">
									<label>Alamat </label>
									<input type="text" name="alamat" placeholder="alamat " required="" autofocus="" value="<?php echo $key->alamat ?>" class="form-control">
								</div>
								<button type="submit" class="btn btn-primary pull-right">Update anggota</button>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				</div>
			</div>
		</div>    	
    	<?php
    }}
 ?>

-->



<?php if($this->session->userdata('level')=="a"){?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active"><?php echo $title ?></li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $title ?></h1>
		</div>
	</div><!--/.row batas untuk pencarian anggota-->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			
				
			<div class="panel-body">
				<?php 
				if ($this->session->flashdata('error')!==null) {
					?>
					<div class="alert alert-danger">
						<?php echo $this->session->flashdata('error') ?>
					</div>
					<?php
				}

				if ($this->session->flashdata('success')!==null) {
					?>
					<div class="alert alert-success">
						<?php echo $this->session->flashdata('success') ?>
					</div>
					<?php
				}
				 ?>
				 <?php if (validation_errors()) : ?>
				      <div class="alert alert-danger">
				        Username telah digunakan
				      </div>
				  <?php endif; ?>
				 
				<table class="table table-hover table-bordered">
                    <tr>
                      <th>Nomor Pendaftaran</th>
                      <th>Nama Pemohon</th>
                      <th>Jenis Permohonan</th>
                      <th colspan="2">Aksi</th>
                    </tr>
                    <?php
                      if ($offset == "") { $i = 0; } else { $i = $offset; }
                      foreach ($query as $row) {
                          $i++;
                          ?>
                          <tr>
                            <td><?php echo $row->no_pendaftaran?></td>
                            <td><?php echo $row->nama_pemohon?></td>
                            <td><?php echo "BNRT"?></td>
                            <td>
                       		<button type="submit" class="btn btn-primary pull-left">Kirim Verifikasi</button
                       		></td><td><button type="submit" class="btn btn-primary pull-left">Survei</button></td>
                       	
                          </tr>
                      <?php
                      }
                      if($query==NULL){
                      ?>
                      <tr>
                        <td colspan="8"> <center>Tidak Ada Data</center> </td>
                      </tr>
                      <?php
                      }
                      ?>
                   </table>
				   <?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>
</div><!--/.row-->
</div>
<?php
}?>