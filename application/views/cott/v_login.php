<div class="row">
	<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
		<div class="login-panel panel panel-default">
			<div class="panel-heading">LOGIN</div>
			<div class="panel-body">
				<?php if (!empty(validation_error())) : ?>
					<div class="alert alert-danger">
						<?php echo validation_error() ?>
					</div>
				<?php endif; ?>
				<form role="form" action="<?php echo site_url('login/dologin') ?>" method="post">
					<fieldset>
						<div class="form-group">
							<input class="form-control" placeholder="username" name="username" type="text" autofocus=""  required="">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Password" name="password" type="password" value="" required="">
						</div>
						<button type="submit" class="btn btn-primary">Login</button>
						<span style="float:right;padding-top: 7px;"><a href="<?php echo site_url('login/lupaPass') ?>" style="color:black;" href="#">Forgot Password?</a>
						</span>
					</fieldset>
				</form>
			</div>
		</div>
	</div><!-- /.col-->
</div><!-- /.row -->	
<div style="text-align:center;font-weight:bold;">
	Not registered? <a href="<?php echo site_url('login/signUp') ?>"><b>Sign Up</b></a>
</div>
