<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active"><?php echo $title ?></li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Upload Survey</h2>
		</div>
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Detail Permohonan</h4>
				</div>
				<div class="panel-body">
					<form class="form-horizontal" method="POST" action="<?php echo base_url('survey/upload_survey/'.$permohonan['id_permohonan']) ?>" enctype="multipart/form-data">
					  <div class="form-group">
					    <label for="nama_lengkap" class="col-sm-2 control-label">Nama Lengkap Pemohon</label>
					    <div class="col-sm-10">
					      <input type="text" name="nama_lengkap" class="form-control" readonly value="<?php echo $permohonan['nama_lengkap'] ?>">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="memiliki_imb" class="col-sm-2 control-label">Memiliki IMB?</label>
					    <div class="col-sm-10">
					      <input type="text" name="memiliki_imb" class="form-control" readonly value="<?php echo $permohonan['memiliki_imb'] ?>">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="bentuk_perusahaan" class="col-sm-2 control-label">Bentuk Perusahaan</label>
					    <div class="col-sm-10">
					      <input type="text" name="bentuk_perusahaan" class="form-control" readonly value="<?php echo $permohonan['bentuk_perusahaan'] ?>">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="no_identitas" class="col-sm-2 control-label">Nomor Identitas</label>
					    <div class="col-sm-10">
					      <input type="text" name="no_identitas" class="form-control" readonly value="<?php echo $permohonan['no_identitas'] ?>">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="alamat" class="col-sm-2 control-label">Alamat</label>
					    <div class="col-sm-10">
					      <input type="text" name="alamat" class="form-control" readonly value="<?php echo $permohonan['alamat'] ?>">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="nama_bangunan" class="col-sm-2 control-label">Nama Bangunan</label>
					    <div class="col-sm-10">
					      <input type="text" name="nama_bangunan" class="form-control" readonly value="<?php echo $permohonan['nama_bangunan'] ?>">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="upload_survey" class="col-sm-2 control-label">Upload Berkas Survey</label>
					    <div class="col-sm-10">
					      <input type="file" name="upload_survey" class="form-control">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="upload_survey" class="col-sm-2 control-label"></label>
					    <div class="col-sm-10">
					      <button class="btn btn-primary">Upload Survey</button>
					    </div>
					  </div>
					</form>
				</div>
			</div>
		</div>
	</div><!--/.row-->
</div>	<!--/.main-->