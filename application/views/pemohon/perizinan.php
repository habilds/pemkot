<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active"><?php echo $title ?></li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Perizinan</h2>
		</div>
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Form Perizinan</h4>
				</div>
				<div class="panel-body">
					<form class="row" action="<?php echo base_url('pemohon/post_perizinan') ?>" method="POST">
						<div class="col-md-6">
							<div class="form-group">
								<label for="nama_lengkap">Nama Lengkap</label>
								<input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap">
							</div>
							<div class="form-group">
								<label for="no_ktp">Nomor KTP</label>
								<input type="text" class="form-control" name="no_ktp" placeholder="Nomor KTP">
							</div>
							<div class="form-group">
								<label for="provinsi">Provinsi</label>
								<input type="text" class="form-control" name="provinsi" placeholder="Provinsi">
							</div>
							<div class="form-group">
								<label for="alamat">Alamat</label>
								<input type="text" class="form-control" name="alamat" placeholder="Alamat">
							</div>
							<div class="form-group">
								<label for="no_hp">Nomor HP</label>
								<input type="text" class="form-control" name="no_hp" placeholder="Nomor HP">
							</div>
							<div class="form-group">
								<label for="email">Email</label>
								<input type="text" class="form-control" name="email" placeholder="Email">
							</div>
							<div class="form-group text-right">
								<label for=""></label>
								<a href="<?php echo base_url('pemohon/status_perizinan') ?>" class="btn btn-secondary">Batal</a>
								<button class="btn btn-primary">Simpan</button>
							</div>
						</div>
					</form>
					
				</div>
			</div>
		</div>
	</div><!--/.row-->
</div>	<!--/.main-->