<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active"><?php echo $title ?></li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Permohonan</h2>
		</div>
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Daftar Permohonan</h4>
				</div>
				<div class="panel-body">
					<?php if (empty($permohonan)): ?>
						<h2>Belum ada permohonan</h2>
					<?php else: ?>
						<div class="table-responsive">
							<table id="dttb" class="table table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Pemohon</th>
										<th>Memiliki IMB</th>
										<th>Bentuk Perusahaan</th>
										<th>Nomor Identitas</th>
										<th>Alamat</th>
										<th>Nama Bangunan</th>
										<th>Permohonan IMB</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($permohonan as $key => $p): ?>
										<tr>
										<td><?php echo $key+1 ?></td>
										<td><?php echo $p['nama_lengkap'] ?></td>
										<td><?php echo $p['memiliki_imb'] ?></td>
										<td><?php echo $p['bentuk_perusahaan'] ?></td>
										<td><?php echo $p['no_identitas'] ?></td>
										<td><?php echo $p['alamat'] . ' ' . $p['kecamatan'] ?></td>
										<td><?php echo $p['nama_bangunan'] ?></td>
										<td><?php echo $p['permohonan_imb'] ?></td>
										<td>
											<a href="<?php echo base_url('pemohon/status_permohonan/'.$p['id_permohonan']) ?>" class="btn btn-primary btn-sm">Detail Permohonan</a>
										</td>
									</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					<?php endif ?>
					
				</div>
			</div>
		</div>
	</div><!--/.row-->
</div>	<!--/.main-->