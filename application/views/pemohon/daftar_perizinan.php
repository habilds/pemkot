<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active"><?php echo $title ?></li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Perizinan</h2>
		</div>
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Daftar Perizinan</h4>
				</div>
				<div class="panel-body">
					<?php if (empty($perizinan)): ?>
						<h2>Belum ada perizinan</h2>
					<?php else: ?>
						<div class="table-responsive">
							<table id="dttb" class="table table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Lengkap</th>
										<th>Nomor KTP</th>
										<th>Provinsi</th>
										<th>Alamat</th>
										<th>No HP</th>
										<th>Email</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($perizinan as $key => $p): ?>
										<tr>
										<td><?php echo $key+1 ?></td>
										<td><?php echo $p['nama_lengkap'] ?></td>
										<td><?php echo $p['no_ktp'] ?></td>
										<td><?php echo $p['provinsi'] ?></td>
										<td><?php echo $p['alamat'] ?></td>
										<td><?php echo $p['no_hp'] ?></td>
										<td><?php echo $p['email'] ?></td>
									</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					<?php endif ?>
					
				</div>
			</div>
		</div>
	</div><!--/.row-->
</div>	<!--/.main-->