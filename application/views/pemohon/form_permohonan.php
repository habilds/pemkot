<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active"><?php echo $title ?></li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Permohonan</h2>
		</div>
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Form Permohonan</h4>
				</div>
				<div class="panel-body">
					
					<form class="row" action="<?php echo base_url('pemohon/post_permohonan') ?>" method="POST">
						<div class="col-md-6">
							<div class="form-group">
								<label for="memiliki_imb">Memiliki IMB?</label>
								<div>
									<input type="radio" name="memiliki_imb" value="ya"> Ya
								</div>
								<div>
									<input type="radio" name="memiliki_imb" value="tidak"> Tidak
								</div>
							</div>
							<div class="form-group">
								<label for="bentuk_perusahaan">Bentuk Perusahaan</label>
								<select name="bentuk_perusahaan" class="form-control">
									<option value="perorangan">Perorangan</option>
									<option value="badan usaha">Badan Usaha</option>
									<option value="pemerintah">Pemerintah</option>
								</select>
							</div>
							<div class="form-group">
								<label for="kecamatan">Kecamatan</label>
								<input type="text" class="form-control" name="kecamatan" placeholder="Kecamatan">
							</div>
							<div class="form-group">
								<label for="alamat">Alamat</label>
								<input type="text" class="form-control" name="alamat" placeholder="Alamat">
							</div>
							<div class="form-group">
								<label for="no_identitas">Nomor Identitas</label>
								<input type="text" class="form-control" name="no_identitas" placeholder="Nomor Identitas">
							</div>
							<div class="form-group">
								<label for="nama_bangunan">Nama Bangunan</label>
								<input type="text" class="form-control" name="nama_bangunan" placeholder="Nama Bangunan">
							</div>
							<div class="form-group">
								<label for="permohonan_imb">Permohonan IMB</label>
								<select name="permohonan_imb" class="form-control">
									<option value="rt">Mendirikan Bangunan RT</option>
									<option value="non rt">Mendirikan Bangunan non RT</option>
								</select>
							</div>
							<div class="form-group">
								<label for="luas_bangunan">Luas Bangunan</label>
								<input type="text" class="form-control" name="luas_bangunan" placeholder="Luas Bangunan">
							</div>
							<div class="form-group">
								<label for="jumlah_lantai">Jumlah Lantai</label>
								<input type="text" class="form-control" name="jumlah_lantai" placeholder="Jumlah Lantai">
							</div>
							<div class="form-group">
								<label for="tinggi_bangunan">Tinggi Bangunan</label>
								<input type="text" class="form-control" name="tinggi_bangunan" placeholder="Tinggi Bangunan">
							</div>
							<div class="form-group text-right">
								<label for=""></label>
								<a href="<?php echo base_url('pemohon/status_permohonan') ?>" class="btn btn-secondary">Batal</a>
								<button class="btn btn-primary">Simpan</button>
							</div>
						</div>
					</form>
					
					
				</div>
			</div>
		</div>
	</div><!--/.row-->
</div>	<!--/.main-->