<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active"><?php echo $title ?></li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Upload Berkas Permohonan</h2>
		</div>
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Upload Berkas Permohonan</h4>
				</div>
				<div class="panel-body">
					<?php if (!empty(validation_errors())): ?>
						<div class="alert alert-danger">
							<?php echo validation_errors(); ?>
						</div>
					<?php endif ?>
					<form class="row" action="<?php echo base_url('pemohon/post_upload_berkas') ?>" method="POST" enctype="multipart/form-data">
						<div class="col-md-6">
							<div class="form-group">
								<label for="id_permohonan">Permohonan</label>
								<select name="id_permohonan" class="form-control">
									<?php foreach ($permohonan as $key => $p): ?>
										<option value="<?php echo $p['id_permohonan'] ?>"><?php echo $p['nama_bangunan'] . ' ' . $p['kecamatan'] ?></option>
									<?php endforeach ?>
								</select>
							</div>
							<div class="form-group">
								<label for="upload_ktp">Upload KTP</label>
								<input type="file" name="upload_ktp" id="upload_ktp" class="form-control">
							</div>
							<div class="form-group">
								<label for="upload_surat_persetujuan_rt">Upload Surat Persetujuan RT</label>
								<input type="file" name="upload_surat_persetujuan_rt" id="upload_surat_persetujuan_rt" class="form-control">
							</div>
							<div class="form-group">
								<label for="upload_sertifikat_tanah">Upload Sertifikat Tanah</label>
								<input type="file" name="upload_sertifikat_tanah" id="upload_sertifikat_tanah" class="form-control">
							</div>
							<div class="form-group">
								<label for="upload_pbb">Upload PBB</label>
								<input type="file" name="upload_pbb" id="upload_pbb" class="form-control">
							</div>
							<div class="form-group">
								<label for="upload_npwp">Upload NPWP</label>
								<input type="file" class="form-control" name="upload_npwp" id="upload_npwp">
							</div>
							<div class="form-group text-right">
								<label for=""></label>
								<a href="<?php echo base_url('pemohon/status_permohonan') ?>" class="btn btn-secondary">Batal</a>
								<button class="btn btn-primary">Simpan</button>
							</div>
						</div>
					</form>
					
				</div>
			</div>
		</div>
	</div><!--/.row-->
</div>	<!--/.main-->