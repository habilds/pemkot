<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active"><?php echo $title ?></li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12" style="display: flex;align-items: center;justify-content: center;flex-direction: column;margin-top: 40px;">
			<div class="text-success" style="font-size: 8em;">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
			</div>
			<h1>Perizinan berhasil dikirim!</h1>
		</div>
	</div><!--/.row-->
</div>	<!--/.main-->