<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo isset($title) ? $title : '' ?></title>

		<link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/datepicker3.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/styles.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/dist/css/dropify.css') ?>" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet">
	</head>

	<body>