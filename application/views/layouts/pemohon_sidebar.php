	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Pemerintahan Kota Palembang</span>&nbsp</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <?php echo $this->session->userdata('username') ?> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?php echo site_url('auth/logout') ?>"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="<?php echo base_url('pemohon') === base_url(uri_string()) ? 'active' : ''; ?>">
				<a href="<?php echo base_url('pemohon') ?>">
					<svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dasbor
				</a>
			</li>
			<li class="parent">
				<a data-toggle="collapse" href="#sub-item-1">
					<svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Perizinan
				</a>
				<ul class="children collapse <?php echo (base_url(uri_string()) == base_url('pemohon/perizinan')) || (base_url(uri_string()) == base_url('pemohon/status_perizinan')) ? 'in' : '' ?>" id="sub-item-1">
					<li>
						<a href="<?php echo base_url('pemohon/perizinan') ?>">
							<svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Form Perizinan
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('pemohon/daftar_perizinan') ?>">
							<svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Lihat Daftar Perizinan
						</a>
					</li>
				</ul>
			</li>
			<li class="parent">
				<a data-toggle="collapse" href="#sub-item-2">
					<svg class="glyph stroked map"><use xlink:href="#stroked-map"/></svg>  Permohonan
				</a>
				<ul class="children collapse <?php echo (base_url(uri_string()) == base_url('pemohon/permohonan')) || (base_url(uri_string()) == base_url('pemohon/status_permohonan')) || (base_url(uri_string()) == base_url('pemohon/upload_berkas')) ? 'in' : '' ?>" id="sub-item-2">
					<li>
						<a href="<?php echo base_url('pemohon/permohonan') ?>">
							<svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Form Permohonan
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('pemohon/upload_berkas') ?>">
							<svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Upload Berkas Permohonan
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('pemohon/daftar_permohonan') ?>">
							<svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Daftar Permohonan
						</a>
					</li>
				</ul>
			</li>
			<li role="presentation" class="divider"></li>
		</ul>
		<div class="attribution">Template by <a href="http://www.medialoot.com/item/lumino-admin-bootstrap-template/">Medialoot</a><br/><a href="http://www.glyphs.co" style="color: #333;">Icons by Glyphs</a></div>
	</div><!--/.sidebar-->