<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active"><?php echo $title ?></li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12" style="margin-bottom: 20px">
			<h2 class="page-header">Detail Permohonan</h2>
			<h4><?php echo $permohonan['tgl_verifikasi'] != null ? '<i style="margin-right: 10px" class="glyphicon glyphicon-ok"></i>' : '<i style="margin-right: 10px" class="glyphicon glyphicon-time"></i>' ?> Verifikasi Berkas</h4>
			<h4><?php echo $permohonan['tgl_survey'] != null ? '<i style="margin-right: 10px" class="glyphicon glyphicon-ok"></i>' : '<i style="margin-right: 10px" class="glyphicon glyphicon-time"></i>' ?></i> Survey</h4>
			<h4><?php echo $permohonan['tgl_approval'] != null ? '<i style="margin-right: 10px" class="glyphicon glyphicon-ok"></i>' : '<i style="margin-right: 10px" class="glyphicon glyphicon-time"></i>' ?> Approval Kabid</h4>
		</div>
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Detail Permohonan</h4>
				</div>
				<div class="panel-body">
					<?php if (empty($berkas)): ?>
						<h2>Pemohon belum mengupload berkas.</h2>
					<?php else: ?>
						<?php foreach ($berkas as $key => $b): ?>
							<div style="margin-bottom: 20px;">
								<h4>Berkas <?php echo str_replace('_', ' ', $b['tipe_berkas']); ?></h4> <a href="<?php echo base_url('uploads/'.$b['lokasi_berkas']); ?>">Download Berkas</a>
							</div>
						<?php endforeach ?>
						<?php if ($permohonan['tgl_verifikasi'] != null): ?>
							Berkas telah diverifikasi
						<?php else: ?>
						<div class="text-center">
							<div>
								<a href="<?php echo base_url('admin/verifikasi_semua_berkas/'.$id_permohonan) ?>" class="btn btn-primary">Verifikasi Berkas</a>
							</div>
							<small>Verifikasi berkas apabila berkas telah lengkap.</small>
						</div>
						<?php endif ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div><!--/.row-->
</div>	<!--/.main-->