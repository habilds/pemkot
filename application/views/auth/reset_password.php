<div class="row">
	<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
		<div class="login-panel panel panel-default">
			<div class="panel-heading">Reset Password</div>
			<div class="panel-body">
				<?php if (!empty(validation_errors()) || null !== $this->session->flashdata('username_check') || null !== $this->session->flashdata('password_check')) : ?>
					<div class="alert alert-danger">
						<?php echo validation_errors() ?>
						<?php echo $this->session->flashdata('username_check') ?>
						<?php echo $this->session->flashdata('password_check') ?>
					</div>
				<?php endif; ?>

				<?php if (!empty(null !== $this->session->flashdata('sukses'))) : ?>
					<div class="alert alert-success">
						<?php echo $this->session->flashdata('sukses') ?>
					</div>
				<?php endif; ?>
				<form role="form" action="" method="post">
					<fieldset>
						<div class="form-group">
							<input class="form-control" placeholder="Password Baru" name="password" type="password" autofocus=""  required="">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Re-type Password Baru" name="passconf" type="password" value="" required="">
						</div>
						<button type="submit" class="btn btn-primary">Reset Password</button>
					</fieldset>
				</form>
			</div>
		</div>
	</div><!-- /.col-->
</div><!-- /.row -->	
<div style="text-align:center;font-weight:bold;">
	Not registered? <a href="<?php echo site_url('auth/register') ?>"><b>Sign Up</b></a>
</div>
