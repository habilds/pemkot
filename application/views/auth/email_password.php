<div class="row">
	<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
		<div class="login-panel panel panel-default">
			<div class="panel-heading">Lupa Password</div>
			<div class="panel-body">
				<?php if (!empty(validation_errors()) || null !== $this->session->flashdata('email_check')) : ?>
					<div class="alert alert-danger">
						<?php echo validation_errors() ?>
						<?php echo $this->session->flashdata('email_check') ?>
					</div>
				<?php endif; ?>
				<form role="form" action="<?php echo site_url('auth/confirm_email') ?>" method="post">
					<fieldset>
						<div class="form-group">
							<input class="form-control" placeholder="Email" name="email" type="text" autofocus=""  required="">
						</div>
						<button type="submit" class="btn btn-primary">Kirim</button>
					</fieldset>
				</form>
			</div>
		</div>
	</div><!-- /.col-->
</div><!-- /.row -->	
<div style="text-align:center;font-weight:bold;">
	Not registered? <a href="<?php echo site_url('auth/register') ?>"><b>Sign Up</b></a>
</div>
