<div class="row">
	<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
		<div class="login-panel panel panel-default">
			<div class="panel-heading">Register</div>
			<div class="panel-body">
				<?php if (!empty(validation_errors()) || null !== $this->session->flashdata('username_check') || null !== $this->session->flashdata('password_check')) : ?>
					<div class="alert alert-danger">
						<?php echo validation_errors() ?>
						<?php echo $this->session->flashdata('username_check') ?>
						<?php echo $this->session->flashdata('password_check') ?>
					</div>
				<?php endif; ?>
				<form role="form" action="<?php echo site_url('auth/post_register') ?>" method="post">
					<fieldset>
						<div class="form-group">
							<input class="form-control" placeholder="Nama lengkap" name="nama_lengkap" type="text" autofocus=""  required="">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Nomor KTP" name="no_ktp" type="text" autofocus=""  required="">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Username" name="username" type="text" autofocus=""  required="">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Email" name="email" type="text" autofocus=""  required="">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Password" name="password" type="password" value="" required="">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Alamat" name="alamat" type="text" autofocus=""  required="">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Provinsi" name="provinsi" type="text" autofocus=""  required="">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Nomor HP" name="no_hp" type="text" autofocus=""  required="">
						</div>
						<button type="submit" class="btn btn-primary">Daftar</button>
						<span style="float:right;padding-top: 7px;"><a href="<?php echo site_url('login/lupaPass') ?>" style="color:black;" href="#">Forgot Password?</a>
						</span>
					</fieldset>
				</form>
			</div>
		</div>
	</div><!-- /.col-->
</div><!-- /.row -->	
<div style="text-align:center;font-weight:bold;">
	already Registered? <a href="<?php echo site_url('/') ?>"><b>Login</b></a>
</div>
