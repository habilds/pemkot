<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function getUserInfo($id)
    {
        $q = $this->db->get_where('tbl_user', array('id' => $id), 1);
        if($this->db->affected_rows() > 0){
            $row = $q->row();
            return $row;
        }else{
            error_log('no user found getUserInfo('.$id.')');
            return false;
        }
    }

    public function getUserInfoByEmail($email){
        $q = $this->db->get_where('tbl_user', array('email' => $email), 1);
        if($this->db->affected_rows() > 0){
            $row = $q->row();
            return $row;
        }
    }

    public function insertToken($user_id)
    {
        $token = substr(sha1(rand()), 0, 30);
        $date = date('Y-m-d H:i:s');

        $string = array(
            'token'=> $token,
            'id_user'=>$user_id,
            'created_at'=>$date
        );
        $query = $this->db->insert_string('tokens',$string);
        $this->db->query($query);
        return $token . $user_id;

    }

    public function isTokenValid($token)
    {
        $tkn = substr($token,0,30);
        $uid = substr($token,30);

        $q = $this->db->get_where('tokens', array(
            'tokens.token' => $tkn,
            'tokens.id_user' => $uid), 1);

        if($this->db->affected_rows() > 0){
            $row = $q->row();

            $user_info = $this->getUserInfo($row->id_user);
            return $user_info;

        }else{
            return false;
        }

    }

    public function updatePassword($post)
    {
        $this->db->where('id', $post['id_user']);
        $this->db->update('tbl_user', array('password' => $post['password']));
        return true;
    }

}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */