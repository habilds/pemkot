<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function check_username($username)
	{
		$this->db->where('username', $username);
		return $this->db->get('tbl_user')->num_rows() > 0;
	}

	public function check_password($username, $password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		return $this->db->get('tbl_user')->num_rows() > 0;
	}

	public function get($username, $password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		return $this->db->get('tbl_user')->row_array(1);
	}

	public function get_pemohon($id_user)
	{
		$this->db->where('id_user', $id_user);
		return $this->db->get('tbl_pemohon')->row_array(1);
	}

	public function daftar($data)
	{
		$data_user = [
			'username' => $data['username'],
			'password' => $data['password'],
			'email' => $data['email'],
			'role' => 2,
		];

		$this->db->insert('tbl_user', $data_user);

		return $this->db->insert_id();
	}

	public function daftar_pemohon($data, $id_user)
	{
		$data_pemohon = [
			'nama_lengkap' => $data['nama_lengkap'],
			'no_ktp' => $data['no_ktp'],
			'alamat' => $data['alamat'],
			'no_hp' => $data['no_hp'],
			'provinsi' => $data['provinsi'],
			'id_user' => $id_user,
		];

		$this->db->insert('tbl_pemohon', $data_pemohon);

		return $this->db->affected_rows() > 0;
	}

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */