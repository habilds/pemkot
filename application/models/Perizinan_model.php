<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perizinan_model extends CI_Model {

	public function get()
	{
		return $this->db->get('tbl_perizinan')->result();
	}

	public function post(array $data)
	{
		$this->db->insert('tbl_perizinan', $data);

		return $this->db->affected_rows() > 0;
	}

	public function get_by_pemohon($id_pemohon)
	{
		return $this->db
					->where('tbl_perizinan.id_pemohon', $id_pemohon)
					->get('tbl_perizinan')
					->result_array();
	}

}

/* End of file Perizinan_model.php */
/* Location: ./application/models/Perizinan_model.php */