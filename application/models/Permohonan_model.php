<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permohonan_model extends CI_Model {

	public function get($id_permohonan = null)
	{
		if ($id_permohonan == null) {
			return $this->db
						->join('tbl_pemohon', 'tbl_pemohon.id_pemohon=tbl_permohonan.id_pemohon')
						->get('tbl_permohonan')
						->result_array();
		}

		return $this->db
					->join('tbl_pemohon', 'tbl_pemohon.id_pemohon=tbl_permohonan.id_pemohon')
					->where('id_permohonan', $id_permohonan)
					->get('tbl_permohonan')
					->result_array();
	}

	public function get_by_pemohon($id_pemohon)
	{
		return $this->db
					->join('tbl_pemohon', 'tbl_pemohon.id_pemohon=tbl_permohonan.id_pemohon')
					->where('tbl_permohonan.id_pemohon', $id_pemohon)
					->get('tbl_permohonan')
					->result_array();
	}

	public function get_berkas_by_permohonan($id_permohonan)
	{
		return $this->db
					->where('id_permohonan', $id_permohonan)
					->get('tbl_berkas_permohonan')
					->result_array();
	}

	public function get_not_survey()
	{
		return $this->db
					->join('tbl_pemohon', 'tbl_pemohon.id_pemohon=tbl_permohonan.id_pemohon')
					->where('tgl_verifikasi IS NOT NULL', null, false)
					->where('tgl_survey IS NULL', null, false)
					->get('tbl_permohonan')
					->result_array();
	}

	public function get_not_approved()
	{
		return $this->db
					->join('tbl_pemohon', 'tbl_pemohon.id_pemohon=tbl_permohonan.id_pemohon')
					->where('tgl_verifikasi IS NOT NULL', null, false)
					->where('tgl_survey IS NOT NULL', null, false)
					->where('tgl_approval IS NULL', null, false)
					->get('tbl_permohonan')
					->result_array();
	}

	public function post(array $data)
	{
		$this->db->insert('tbl_permohonan', $data);

		return $this->db->affected_rows() > 0;
	}

	public function upload_files($datas)
	{
		$this->db->insert_batch('tbl_berkas_permohonan', $datas);

		return $this->db->affected_rows() > 0;
	}

}

/* End of file Permohonan_model.php */
/* Location: ./application/models/Permohonan_model.php */